<form action="$Link" role="search">
    <% include FAQCategorySelector %>
    <input id="FAQSearchForm_FAQSearchForm_Search" type="search" name="$SearchTermKey" placeholder="$SearchFieldPlaceholder" value="$Query" title="$SearchFieldPlaceholder" />
    <input type="submit" value="$SearchButtonText" />
</form>
